def buildApp () {
    echo "Start building app."
    sh "mvn package"
}

def buildDockerImage () {
    echo "Start login to Dockerhub, build image and push to Dockerhub"
    withCredentials([usernamePassword(credentialsId:'dockerhub_login',passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t felix3105/testjenkins:1.0 ."
        sh " echo $PASS | docker login -u $USER --password-stdin "
        sh " docker push felix3105/testjenkins:1.0 "
    }
} 

def deploy () {
    echo "Deploying application to server"
}

return this
